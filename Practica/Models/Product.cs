namespace Practica.Models;

public class Product
{
    public int Id { get; set; }
    public string Title { get; set; }
    public Decimal Price { get; set; }
    public string Description { get; set; }
    public List<string> Images { get; set; }

    
    // Relación con categoría
    public int CategoryId { get; set; }
    public Category Category { get; set; }
    
}