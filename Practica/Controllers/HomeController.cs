﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Practica.Models;

namespace Practica.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    public async Task<IActionResult> Index()
    {
        using (HttpClient client = new HttpClient())
        {
            client.BaseAddress = new Uri("https://api.escuelajs.co/api/v1/");

            HttpResponseMessage response = await client.GetAsync("products?offset=0&limit=10");

            if (response.IsSuccessStatusCode)
            {
                List<Product> result = await response.Content.ReadFromJsonAsync<List<Product>>();

                for (int i = 0; i < result.Count; i++)
                {
                    Console.WriteLine(result[i].Title);
                }
                return View(result);
            }
            else
            {
                return View("Error");
            }
        }
    }



    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}